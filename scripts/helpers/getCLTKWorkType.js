import fs from 'fs';
import path from 'path';

import * as models from '../../src/models/index.js';

const repositoriesFile = fs.readFileSync(path.join('.', 'repositories.json'));
const repositoriesJSON = JSON.parse(repositoriesFile);

const _getDefaultLanguageString = (title) => {
	let defaultLanguage;
	repositoriesJSON.repositories.map((repo) => {
		if (repo.title === title) {
			defaultLanguage = repo.default_language;
		}
	});
	return defaultLanguage;
};

const getCLTKWorkType = async (work) => {
	const tg = await work.getTextgroup();
	const col = await tg.getCollection();

	if (!col) {
		return 'edition';
	}

	const defaultLanguage = _getDefaultLanguageString(col.title);

	const filename = work.filename.split('/').pop();
	if (filename.includes(defaultLanguage)) {
		return 'edition';
	}
	return 'translation';
};

export default getCLTKWorkType;

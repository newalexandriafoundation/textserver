import stripTags from 'underscore.string/stripTags';
import trim from 'underscore.string/trim';
import Sequelize from 'sequelize';

import dotenvSetup from '../src/dotenv.js';
import * as models from '../src/models/index.js';
import db, { dbSetup } from '../src/db.js';
import logger from '../src/logger.js';

// setup environment variables and db connection
dotenvSetup();
dbSetup();

const normalizeTextnodes = async () => {
	try {
		console.log(process.argv);
		let start = parseInt(process.argv[2], 10);
		console.log(`[INFO] starting from page ${start}`);

		// get all non-english language ids
		const languageIds = await models.Language.findAll({
			attributes: ['id'],
			where: {
				slug: {
					[Sequelize.Op.ne]: 'english',
				},
			},
		}).map((l) => l.id);
		// get all work ids that are not english works
		const workIds = await models.Work.findAll({
			attributes: ['id'],
			where: {
				languageId: {
					[Sequelize.Op.in]: languageIds,
				},
			},
		}).map((w) => w.id);
		console.log(`# of non-english work ids ${workIds.length}`);
		// get all textnodes without normalized texts
		const pageSize = 1000;
		let processed = 0;
		let skipped = 0;

		while (1) {
			start += 1;
			console.log(`[INFO] page # ${start}`);
			const textnodes = await models.TextNode.findAll(
				// eslint-disable-line
				{
					limit: pageSize,
					offset: start * pageSize,
				}
			);
			const nodeCount = textnodes.length;
			console.log(`[INFO] # of textndoes to be processed ${nodeCount}`);
			if (nodeCount < 1) {
				break;
			}

			for (let i = 0; i < textnodes.length; i += 1) {
				const tn = textnodes[i];
				if (
					tn.normalized_text === null &&
					workIds.includes(tn.workId)
				) {
					const strippedText = trim(stripTags(tn.text));
					const normalizedText = String(strippedText)
						.normalize('NFD')
						.replace(/[\u0300-\u036f]/g, '');
					// console.log(`Original text: ${strippedText}`);
					// console.log('------');
					// console.log(`Normalized text: ${normalizedText}`);
					// console.log('------');
					tn.normalized_text = normalizedText;
					await tn.save(); // eslint-disable-line
					processed += 1;
					// console.log(`Normalized text: ${tn.normalized_text}`);
				} else {
					skipped += 1;
					// console.log(`Original text: ${tn.text}`);
				}
			}

			console.log(`[INFO] processed: ${processed} skipped: ${skipped}`);
			// if (start >= 10) break;
			// break;
		}

		/* eslint-disable */
		/*
		for (const workId of workIds) {
			const tns = await models.TextNode.findAll(
				{
					where: {
						workId,
						normalized_text: null,
					},
				},
			);
			console.log(`# of textNodes for work ${workId} to be normalized ${tns.length}`);

			for (const tn of tns) {
				const strippedText = trim(stripTags(tn.text));
				const normalizedText = String(strippedText)
					.normalize('NFD')
					.replace(/[\u0300-\u036f]/g, '');

				//console.log(`Original text: ${strippedText}`);
				//console.log('------');
				//console.log(`Normalized text: ${normalizedText}`);
				//console.log('------');

				tn.normalized_text = normalizedText;

				await tn.save();
			}

			// break;
			

		}*/
		/* eslint-enable */
		return '[INFO] Done.';
	} catch (e) {
		logger.error(e);
		return 'Error, Terminated.';
	}
};

db.authenticate().then(async () => {
	// sync database
	await db.sync();

	// run
	logger.info('Beginning normalizeTextnodes');
	const result = await normalizeTextnodes();
	logger.info(result);

	// close db
	db.close();
});

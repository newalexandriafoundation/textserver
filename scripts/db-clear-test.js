import dotenv from 'dotenv';

import dotenvSetup from '../src/dotenv.js';
import * as Models from '../src/models/index.js';
import db, { dbSetup } from '../src/db.js';
import logger from '../src/logger.js';

// setup environment variables and db connection
dotenvSetup();
dbSetup();

// safety belt
const expectedTestDBName = 'textserver_test';
if (db.connectionManager.config.database !== expectedTestDBName) {
	throw new Error('Not operating on test DB, terminated.');
}

db.authenticate().then(async () => {
	// sync database
	const sync = await db.sync();

	// destory all
	logger.info('Dropping all tables in database');
	await Promise.all(
		Object.keys(Models).map(async (modelName) => {
			await Models[modelName].destroy({
				where: {},
			});
		})
	);

	// close db
	return db.close();
});

import _s from 'underscore.string';
import deepfilter from 'deep-filter';
import { Op } from 'sequelize';

// logic
import PermissionsService from './PermissionsService.js';

import db from '../../db.js';

// models
import TextNode from '../../models/textNode.js';
import Work from '../../models/work.js';
import Language from '../../models/language.js';
import Version from '../../models/version.js';
import Exemplar from '../../models/exemplar.js';
import Translation from '../../models/translation.js';
import RefsDecl from '../../models/refsDecl.js';

// lib
import serializeUrn from '../../modules/cts/lib/serializeUrn.js';

const ASCII_REGEX = /^\w+$/;

const normalizeText = (s) =>
	String(s)
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '');

/**
 * @param {number} workId - the textNode.workId to constrain the search
 * @param {string} textSearch - the search string
 * @returns {number}
 */
async function countTextNodesMatchingFilterWithWorkId(workId, textSearch) {
	const work = await Work.findOne({ where: { id: workId } });
	const lang = await Language.findOne({
		where: { id: work.languageId },
	});

	if (lang.slug === 'english') {
		const results = await db.query(
			`
			SELECT COUNT(id) FROM ${TextNode.tableName}
			WHERE _search @@ plainto_tsquery('english', :query)
			AND "workId" = :workId
			`,
			{
				model: TextNode,
				replacements: {
					query: textSearch,
					workId,
				},
			}
		);

		return results[0].dataValues.count;
	}

	const results = await TextNode.count({
		where: {
			normalized_text: {
				[Op.iLike]: `%${normalizeText(textSearch)}%`,
			},
			workId,
		},
	});

	return results;
}

/**
 * @param {string} textSearch - the search string
 * @returns {number}
 */
async function countTextNodesMatchingFilter(textSearch) {
	if (ASCII_REGEX.test(textSearch)) {
		const results = await db.query(
			`
			SELECT COUNT(id) FROM ${TextNode.tableName}
			WHERE _search @@ plainto_tsquery('english', :textSearch)
			`,
			{
				model: TextNode,
				replacements: {
					textSearch,
				},
			}
		);

		return results[0].dataValues.count;
	}

	return TextNode.count({
		where: {
			normalized_text: {
				[Op.iLike]: `%${textSearch}%`,
			},
		},
	});
}

/**
 * @param {number} workId The textNode.workId to constrain the search
 * @param {string} textSearch The search string
 * @param {number} opts.after The offset/cursor. Ignored if `opts.before` is present.
 * @param {number} opts.before The offset/cursor.
 * @param {number} opts.first The limit
 * @param {number} opts.last The limit
 * @returns {Object}
 */
async function findTextNodesMatchingFilterWithWorkId(
	workId,
	textSearch,
	{ after = 0, before = 0, first = 30, last = 30 }
) {
	const work = await Work.find({ where: { id: workId } });
	const lang = await Language.find({
		where: { id: work.languageId },
	});

	if (lang.slug === 'english') {
		const query =
			before > 0
				? `
			SELECT * FROM (
				SELECT * FROM ${TextNode.tableName}
				WHERE _search @@ plainto_tsquery('english', :textSearch)
				AND "workId" = :workId
				AND id < :before
				ORDER BY id DESC
				LIMIT :last
			) as t ORDER BY id ASC;`
				: `
			SELECT * FROM ${TextNode.tableName}
			WHERE _search @@ plainto_tsquery('english', :textSearch)
			AND id > :after
			AND "workId" = :workId
			ORDER BY id ASC
			LIMIT :first;
			`;
		const results = await db.query(query, {
			model: TextNode,
			replacements: {
				after,
				before,
				first,
				last,
				textSearch,
				workId,
			},
		});

		return results;
	}

	let limit = first;
	let order = [['id', 'ASC']];
	let id = { [Op.gt]: after };

	if (before > 0) {
		limit = last;
		order = [['id', 'DESC']];
		id = { [Op.lt]: before };
	}

	const results = await TextNode.findAll({
		limit,
		order,
		where: {
			id,
			normalized_text: {
				[Op.iLike]: `%${normalizeText(textSearch)}%`,
			},
			workId,
		},
	});

	if (before > 0) {
		return results.reverse();
	}

	return results;
}

/**
 * @param {string} textSearch The search string
 * @param {number} opts.after The offset/cursor. Ignored if `opts.before` is present.
 * @param {number} opts.before The offset/cursor.
 * @param {number} opts.first The limit
 * @param {number} opts.last The limit
 * @returns {Object}
 */
async function findTextNodesMatchingFilter(
	textSearch,
	{ after = 0, before = 0, first = 30, last = 30 }
) {
	if (ASCII_REGEX.test(textSearch)) {
		const query =
			before > 0
				? `
			SELECT * FROM (
				SELECT * FROM ${TextNode.tableName}
				WHERE _search @@ plainto_tsquery('english', :textSearch)
				AND id < :before
				ORDER BY id DESC
				LIMIT :last
			) as t ORDER BY id ASC;`
				: `
			SELECT * FROM ${TextNode.tableName}
			WHERE _search @@ plainto_tsquery('english', :textSearch)
			AND id > :after
			ORDER BY id ASC
			LIMIT :first;
			`;
		const results = await db.query(query, {
			model: TextNode,
			replacements: {
				after,
				before,
				first,
				last,
				textSearch,
			},
		});

		return results;
	}

	let limit = first;
	let order = [['id', 'ASC']];
	let id = { [Op.gt]: after };

	if (before > 0) {
		limit = last;
		order = 'DESC';
		id = { [Op.lt]: before };
	}

	const results = await TextNode.findAll({
		group: 'workId',
		limit,
		order,
		where: {
			id,
			normalized_text: {
				[Op.iLike]: `%${normalizeText(textSearch)}%`,
			},
		},
	});

	if (before > 0) {
		return results.reverse();
	}

	return results;
}

const parseUrnToQuery = async (urn, language, workId) => {
	const originalUrn = { ...urn };
	let works = [];

	const query = {
		order: ['index'],
		where: {},
	};

	if (workId) {
		query.include = [
			{
				model: Work,
				where: {
					id: workId,
				},
			},
		];
	} else {
		const workUrn = serializeUrn({
			ctsNamespace: urn.ctsNamespace,
			textGroup: urn.textGroup,
			work: urn.work,
		});

		const workQuery = {
			where: {
				urn: workUrn,
			},
		};

		if (language) {
			workQuery.include = [
				{
					model: Language,
					where: {
						slug: language,
					},
				},
			];
		}

		const version = urn.version;
		delete urn.version;

		const exemplar = urn.exemplar;
		delete urn.exemplar;

		const translation = urn.translation;
		delete urn.translation;

		/**
		 * when version/exemplar/translation exists,
		 * the URN represented a single Work of that version/exemplar/translation
		 * thus the query should replace workIds with a single workId of that when version/exemplar/translation
		 *
		 * Interesting use case with CTS URNs and perhaps something to address in the future
		 * querying by verison, exemplar, and translation, but only in that order
		 */
		if (version) {
			// retrieve work id of this version
			const urnWithoutPassage = urn;
			delete urnWithoutPassage.passage; // to make sure urn for the version parses correctly
			const versionURN = `${serializeUrn(urnWithoutPassage)}.${version}`;
			const versionRecord = await Version.findOne({
				where: {
					urn: versionURN,
				},
			});
			if (versionRecord) {
				workQuery.where = {
					id: versionRecord.workId,
				};
			} else {
				// safety check for incorrectly parsed version or non-exist version
				const workRecord = await Work.findOne({
					where: {
						full_urn: versionURN,
					},
				});

				if (workRecord) {
					workQuery.where = {
						id: workRecord.id,
					};
				} else {
					return false;
				}
			}

			if (exemplar) {
				const exemplarRecord = await Exemplar.findOne({
					where: {
						urn: `${serializeUrn(urn)}.${version}.${exemplar}`,
					},
				});
				if (exemplarRecord) {
					workQuery.where = {
						id: exemplarRecord.workId,
					};
				}

				if (translation) {
					const translationRecord = await Translation.findOne({
						where: {
							urn: `${serializeUrn(
								urn
							)}.${version}.${exemplar}.${translation}`,
						},
					});
					if (translationRecord) {
						workQuery.where = {
							id: translationRecord.workId,
						};
					}
				}
			}
		}

		// find a work via urn
		works = await Work.findAll(workQuery);
		const workIds = [];
		works.forEach((_work) => {
			workIds.push(_work.id);
		});
		if (works && works.length) {
			query.include = [
				{
					model: Work,
					where: {
						id: { [Op.in]: workIds },
					},
				},
			];
		}
	}

	// parse passage to range query
	if (originalUrn.passage && originalUrn.passage.length) {
		query.where.location = originalUrn.passage[0];

		const tn = await TextNode.findOne({
			...query,
			order: [['index', 'DESC']],
		});

		query.where.index = {
			[Op.gte]: tn.index,
		};

		if (originalUrn.passage.length > 1) {
			query.where.location = originalUrn.passage[1];
			const textNodeLast = await TextNode.findOne(query);

			if (textNodeLast) {
				const textNodeSpan = textNodeLast.index - tn.index + 1; // eslint-disable-line
				query.where.index[Op.lte] = textNodeLast.index;
				// The node span is basically meaningless for more than one work,
				// but if the URN specifies a single edition, we should use it.
				query.limit = works.length === 1 ? textNodeSpan : query.limit;
			}
		}

		delete query.where.location;
	}

	return query;
};

/**
 * Logic-layer service for dealing with text nodes
 */
export default class TextNodeService extends PermissionsService {
	/**
	 * Create a text node
	 * @param {Object} textNode - candidate text node to create
	 * @returns {string} id of newly created text node
	 */
	textNodeCreate(textNode) {
		if (this.userIsAdmin) {
			return TextNodes.insert(textNode);
		}
		return new Error('Not authorized');
	}

	/**
	 * Update a text node
	 * @param {Object} id - id of the text node to be modified
	 * @param {Object} textNode - text node input type for updating
	 * @returns {Object} modified text node
	 */
	async textNodeUpdate(id, textNode) {
		if (this.userIsAdmin) {
			const textNodeInstance = await TextNode.findById(id);
			textNodeInstance.updateAttributes(textNode);
			return textNodeInstance.save();
		}
		return new Error('Not authorized');
	}

	/**
	 * Remove a text node
	 * @param {string} textNodeId - id of text node
	 * @returns {boolean} result from mongo orm remove
	 */
	textNodeRemove(id) {
		if (this.userIsAdmin) {
			const removeId = new Mongo.ObjectID(id);
			return TextNodes.remove({ _id: removeId });
		}
		return new Error('Not authorized');
	}

	/**
	 * Get text nodes
	 * @param {string} index
	 * @param {Object} urn
	 * @param {[number]} location
	 * @param {[number]} startsAtLocation
	 * @param {[number]} endsAtLocation
	 * @param {number} startsAtIndex
	 * @param {number} endsAtIndex
	 * @param {number} offset
	 * @param {number} workid
	 * @returns {Object[]} array of text nodes
	 */
	async textNodesGet(
		index,
		urn,
		language,
		location,
		startsAtLocation,
		endsAtLocation,
		startsAtIndex,
		offset,
		pageSize = 30,
		workId,
		filter
	) {
		let textNode = null;
		let query = {
			order: ['index'],
			where: {},
			include: [],
		};

		if (workId) {
			query.include.push({
				model: Work,
				where: {
					id: parseInt(workId, 10),
				},
			});
		} else if (filter && filter.workIds) {
			query.include.push({
				model: Work,
				where: {
					id: {
						[Op.or]: filter.workIds,
					},
				},
			});
		}

		if (location) {
			query.where.location = location;
		}

		if (index) {
			query.where.index = index;
		}

		if (startsAtIndex) {
			query.where.index = {
				[Op.gte]: startsAtIndex,
			};
		}

		if (startsAtIndex && endsAtIndex) {
			query.where.index = {
				[Op.gte]: startsAtIndex,
				[Op.lt]: endsAtIndex,
			};
		}

		if (offset) {
			query.offset = offset;
		}

		if (startsAtLocation) {
			query.where.location = startsAtLocation;
			textNode = await TextNode.findOne(query);

			delete query.where.location;

			if (textNode) {
				query.where.index = {
					[Op.gte]: textNode.index,
				};
			}
		}

		if (pageSize) {
			query.limit = pageSize;
		}

		if (urn) {
			const q = await parseUrnToQuery(urn, language, workId);
			// TODO: better error handling for parsing errors
			// If error parsing URN to query, just return empty array
			if (!q) {
				return [];
			}

			query = {
				...query,
				...q,
			};
		}

		if (!query.limit) {
			query.limit = pageSize;
		}
		const textNodes = await TextNode.findAll(query);

		return textNodes;
	}

	/**
	 * Get paginated text node location following specified location based on offset
	 * 	offset is the size of the page rendered
	 * @param {number} work - id of textnode work
	 * @param {number} index - the index of the pivot node
	 * @param {number[]} location - location array to offset from
	 * @param {number} offset - the offset to iterate
	 * @returns {number[]} location array
	 */
	textLocationNext(workId, index, location, offset = 10) {
		const query = {
			where: {},
			include: [],
			order: ['index'],
		};

		if (workId) {
			query.include.push({
				model: Work,
				where: {
					id: parseInt(workId, 10),
				},
			});
		}

		if (index) {
			query.where.index = {
				[Op.gt]: index,
			};
			query.limit = offset;

			return TextNode.findAll(query).then((nodes) => {
				if (!nodes || !nodes.length) {
					return [];
				}

				return nodes[0].location;
			});
		}

		if (location) {
			query.where.location = location;
		}

		return TextNode.findOne(query).then((node) => {
			delete query.where.location;

			if (!node) return [];

			query.where.index = {
				[Op.gte]: node.index,
			};

			// page size (offset) sanity check
			if (offset < 1) {
				return new Error('Offset(page size) cannot be 0/null.');
			}

			query.limit = offset;

			return TextNode.findAll(query).then((nodes) => {
				if (!nodes || !nodes.length || !node.length === offset) {
					return [];
				}

				return nodes[nodes.length - 1].location;
			});
		});
	}

	/**
	 * Get paginated text node location following specified location based on offset
	 * @param {number} workId - id of textnode work
	 * @param {number} index - the index of the pivot node
	 * @param {number[]} location - location array to offset from
	 * @param {number} offset - the offset to iterate
	 * @returns {number[]} - location array
	 */
	textLocationPrev(workId, index, location, offset = 10) {
		const query = {
			where: {},
			include: [],
			order: ['index'],
		};

		if (workId) {
			query.include.push({
				model: Work,
				where: {
					id: parseInt(workId, 10),
				},
			});
		}

		if (index) {
			query.where.index = {
				[Op.gte]: index - offset < 0 ? 0 : index - offset,
				[Op.lt]: index,
			};
			query.limit = offset;

			return TextNode.findAll(query).then((nodes) => {
				if (!nodes || !nodes.length) {
					return [];
				}

				return nodes[0].location;
			});
		}

		if (location) {
			query.where.location = location;
		}

		return TextNode.findOne(query).then((node) => {
			if (!node) return [];

			delete query.where.location;

			query.where.index = {
				[Op.lt]: node.index,
			};
			query.order = [['index', 'DESC']];
			query.limit = offset;

			return TextNode.findAll(query).then((nodes) => {
				if (!nodes || !nodes.length) {
					return [];
				}

				return nodes[nodes.length - 1].location;
			});
		});
	}

	/**
	 * Get text node urn
	 */
	getTextNodeURN(parent) {
		let urn = '';
		const passageUrn = '';
		let workUrn = '';

		if (parent.dataValues && parent.work && parent.work.dataValues) {
			const location = parent.dataValues.location;

			const work = parent.work.dataValues;
			if (work) {
				workUrn = work.urn;
				urn = `${workUrn}:`;
			}

			urn = `${urn}${location.join('.')}`;
		}

		return urn;
	}

	/**
	 * Get text node words
	 */
	getTextNodeWords(parent) {
		const words = [];

		if (parent.dataValues) {
			const sanitizedText = _s(parent.dataValues.text).stripTags().trim();
			const _words = sanitizedText.split(' ');

			_words.forEach((word) => {
				words.push({
					word,
					parent,
				});
			});
		}

		return words;
	}

	/**
	 * Get word urn
	 */
	getWordURN(parent) {
		let urn = '';
		const passageUrn = '';
		let workUrn = '';

		if (parent.word && parent.parent) {
			const location = parent.parent.dataValues.location;

			const work = parent.parent.work.dataValues;
			if (work) {
				workUrn = work.urn;
				urn = `${workUrn}:`;
			}

			urn = `${urn}${location.join('.')}@${parent.word}`;
		}

		return urn;
	}

	/**
	 * Count the number of textNodes that satisfy the provided filter
	 * @param {Object} filter
	 * @param {string} filter.textSearch - The search string
	 * @param {number} filter.workId - The work to which these textNodes must belong
	 */
	async count({ textSearch, workId } = {}) {
		if (workId) {
			return countTextNodesMatchingFilterWithWorkId(workId, textSearch);
		}
		return countTextNodesMatchingFilter(textSearch);
	}

	/**
	 * Search for textNodes that satisfy the provided filter
	 * @param {Object} filter
	 * @param {number} filter.after - A cursor for the starting point (exclusive) of the ordered results
	 * @param {number} filter.before - A cursor for the ending point (exclusive) of the ordered results
	 * @param {number} filter.first - The number of textNodes to return
	 * @param {number} filter.last - The number of textNodes to return (with filter.before)
	 * @param {string} filter.textSearch - The search string
	 * @param {number} filter.workId - The work to which these textNodes must belong
	 *
	 * @returns {Array} an array of matching textNodes
	 */
	async search({ after, before, first, last, textSearch, workId } = {}) {
		if (workId) {
			return findTextNodesMatchingFilterWithWorkId(workId, textSearch, {
				after,
				before,
				first,
				last,
			});
		}
		return findTextNodesMatchingFilter(textSearch, {
			after,
			before,
			first,
			last,
		});
	}

	/**
	 *
	 * @param {Object} textNode
	 * @param {number} textNode.id - The id of the last textNode in the current result set
	 * @param {string} filter.textSearch - The search string
	 * @param {number} filter.workId - The work to which these textNodes must belong
	 *
	 * @returns {boolean}
	 */
	async searchHasNextPage({ id }, { textSearch, workId }) {
		let results = [];

		const opts = {
			after: id,
			first: 1,
		};

		if (workId) {
			results = await findTextNodesMatchingFilterWithWorkId(
				workId,
				textSearch,
				opts
			);
		} else {
			results = await findTextNodesMatchingFilter(textSearch, opts);
		}

		return results.length > 0;
	}

	/**
	 *
	 * @param {Object} textNode
	 * @param {number} textNode.id - The id of the first textNode in the current result set
	 * @param {string} filter.textSearch - The search string
	 * @param {number} filter.workId - The work to which these textNodes must belong
	 *
	 * @returns {boolean}
	 */
	async searchHasPreviousPage({ id }, { textSearch, workId }) {
		let results = [];

		const opts = {
			before: id,
			last: 1,
		};

		if (workId) {
			results = await findTextNodesMatchingFilterWithWorkId(
				workId,
				textSearch,
				opts
			);
		} else {
			results = await findTextNodesMatchingFilter(textSearch, opts);
		}

		return results.length > 0;
	}

	/**
	 * Generate table of content as a list of nested nodes with refsDecls meta
	 */
	async generateTableOfContent(workID) {
		// get refsDecls for workID
		const refsDecls = await RefsDecl.findAll({
			where: {
				workId: workID,
			},
			order: [['structure_index', 'ASC']],
			raw: true,
		});
		// produce a mapping of layer to refsDecls label
		const refsDeclsMap = [];
		refsDecls.map((refsDecl) => {
			refsDeclsMap[refsDecl.structure_index] = refsDecl.label;
		});
		// get textnodes for workID
		const textNodes = await TextNode.findAll({
			where: {
				workId: workID,
			},
			attributes: ['location'],
			// limit: 10,
			raw: true,
		});

		/* testers 
		refsDeclsMap =  [ 'book', 'card', 'test' ];

		textNodes = [
			{ location: [ 1, 8, 2 ] }, 
			{ location: [ 1, 5, 3 ] },
		];
		*/

		// init end result table of content
		const tableOfContent = [];
		// process each textNode
		textNodes.map((textNode) => {
			// a pointer to track tree level
			let p = tableOfContent;
			// produce nodes according to its tree level
			for (let i = 0; i < textNode.location.length; i += 1) {
				// the value of the node at the current tree level
				const nodeValue = textNode.location[i];
				// create node if not exist, initialize children array
				if (!p[nodeValue]) {
					p[nodeValue] = {
						label: refsDeclsMap[i],
						index: nodeValue,
						children: [],
					};
				}
				// point to children of the current tree level
				p = p[nodeValue].children;
				// point back to root tree when the textNode is done processing
				if (i === textNode.location.length) {
					p = tableOfContent;
				}
			}
			// console.log('...toc', JSON.stringify(tableOfContentFiltered, null, 4));
		});
		// remove empty nodes from the tree with deepfilter
		const tableOfContentFiltered = deepfilter(
			tableOfContent,
			function (value, prop, subject) {
				return value !== null;
			}
		);
		return tableOfContentFiltered;

		/* test data
		return [
			{
				label: 'book', // model.refsDecls.label
				index: 1, // model.textNode.location[0]
				children: [
					{
						label: 'card',
						index: 1, // model.textNode.location[1]
						children: [], // leaf node has no children
					},
					{
						label: 'card',
						index: 2,
						children: [],
					}
					// ...
				],
			},
			{
				label: 'book',
				index: 2,
				children: [
					{
						label: 'card',
						index: 1,
						children: [],
					},
					{
						label: 'card',
						index: 2,
						children: [],
					}
					// ...
				],
			}
			// ...
		];
		*/
	}
}

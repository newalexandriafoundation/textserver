module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.sequelize.query(
			'ALTER TABLE textnodes ADD COLUMN IF NOT EXISTS normalized_text text'
		);
	},

	down: queryInterface =>
		queryInterface.removeColumn('textnodes', 'normalized_text'),
};

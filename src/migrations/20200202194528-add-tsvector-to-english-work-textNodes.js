/**
 * @prettier
 */

const table = 'textnodes';
const vectorName = '_search';

module.exports = {
	up: queryInterface =>
		queryInterface.sequelize
			.transaction(t =>
				queryInterface.sequelize
					.query(
						`
			ALTER TABLE ${table} ADD COLUMN IF NOT EXISTS ${vectorName} TSVECTOR;`,
						{ transaction: t }
					)
					.then(() =>
						queryInterface.sequelize.query(
							`
			UPDATE textnodes SET ${vectorName} = to_tsvector('english', text)
			FROM works WHERE textnodes."workId" = works.id 
				AND works."languageId" = (SELECT id FROM languages WHERE slug = 'english');`,
							{ transaction: t }
						)
					)
					.then(() =>
						queryInterface.sequelize.query(
							`
			CREATE INDEX IF NOT EXISTS ${table}_${vectorName} ON ${table} USING gin(${vectorName});`,
							{ transaction: t }
						)
					)
					.then(() =>
						queryInterface.sequelize
							.query(
								`
				CREATE TRIGGER ${table}${vectorName}_update
				BEFORE INSERT OR UPDATE ON ${table}
				FOR EACH ROW
					EXECUTE PROCEDURE tsvector_update_trigger(${vectorName}, 'pg_catalog.english', text);
				`,
								{ transaction: t }
							)
							.catch(() => Promise.resolve())
					)
			),

	down: queryInterface =>
		queryInterface.sequelize.transaction(t =>
			queryInterface.sequelize
				.query(
					`DROP TRIGGER IF EXISTS ${table}${vectorName}_update ON ${table};`,
				{
					transaction: t,
				}
				)
				.then(() =>
					queryInterface.sequelize
						.query(
							`
			DROP INDEX IF EXISTS ${table}_${vectorName};`,
							{ transaction: t }
						)
						.then(() =>
							queryInterface.sequelize.query(
								`
				ALTER TABLE ${table} DROP COLUMN ${vectorName};`,
								{ transaction: t }
							)
						)
				)
		),
};

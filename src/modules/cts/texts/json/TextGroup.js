import TextGroup from '../../../../models/textGroup.js';

class _TextGroup {
	constructor({ author, urn }) {
		this.title = author;
		this.urn = urn;
		this._id = null;
		this.works = [];
	}

	/**
	 * Save all textgroup data and work/textNode data in this textgroup
	 */
	async save(collection) {
		const title = this.title || '';
		const urn = this.urn || '';

		// de-dup and save TextGroup
		let textGroup = await TextGroup.findOne({
			where: {
				collectionId: collection.id,
				urn: urn,
			},
		});
		if (textGroup) {
			textGroup = await textGroup.updateAttributes({
				title: this.title,
				urn: this.urn,
			});
		} else {
			textGroup = await TextGroup.create({
				title: this.title,
				urn: this.urn,
			});
		}

		await textGroup.setCollection(collection);
		await collection.addTextgroup(textGroup);
	}
}

export default _TextGroup;

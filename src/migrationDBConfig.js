const dotenvSetup = require('./dotenv');

dotenvSetup();

const host = process.env.DB_HOST;
const password = process.env.DB_PASS;
const username = process.env.DB_USER;

module.exports = {
	development: {
		username: username || 'postgres',
		password: password || 'root',
		database: 'textserver',
		host: host || '127.0.0.1',
		dialect: 'postgres'
	},
	test: {
		username: username || 'runner',
		password: password || null,
		database: 'textserver_test',
		host: process.env.DB_HOST,
		dialect: 'postgres'
	},
	production: {
		username: null,
		password: null,
		database: null,
		host: null,
		dialect: 'postgres'
	}
};
